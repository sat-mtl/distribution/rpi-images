sudo apt install -y tmux git liblo-tools
mkdir ~/sources && cd ~/sources &&\
git clone https://gitlab.com/sat-mtl/distribution/MPU.git &&\
cd ~/sources/MPU/scripts &&\
sudo chmod +x building_script.sh rename_mpu.sh change_ipblock.sh &&\
./building_script.sh &&\
./run_script.sh